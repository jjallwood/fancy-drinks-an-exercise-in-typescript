import {RECIPE_LOAD_COMPLETE, RECIPE_LOAD_ERROR, RECIPE_LOADING} from "./recipeDuck";

export interface RecipeGetResponse {
    drinks: Array<RawRecipe> | string;
}

export interface Recipe {
    id: string;
    name: string;
    category: string;
    alcoholic: string;
    glass: string;
    instructions: string;
    img: string;
    ingredients: Array<RecipeIngredient>;
    dateModified: string;
}

export interface RecipeIngredient {
    name: string;
    quantity?: string;
}

export interface RawRecipe {
    idDrink: string;
    strDrink: string;
    strCategory: string;
    strAlcoholic: string;
    strGlass: string;
    strInstructions: string;
    strDrinkThumb: string;
    strIngredient1: string;
    strIngredient2: string;
    strIngredient3: string;
    strIngredient4: string;
    strIngredient5: string;
    strIngredient6: string;
    strIngredient7: string;
    strIngredient8: string;
    strIngredient9: string;
    strIngredient10: string;
    strIngredient11: string;
    strIngredient12: string;
    strIngredient13: string;
    strIngredient14: string;
    strIngredient15: string;
    strMeasure1: string;
    strMeasure2: string;
    strMeasure3: string;
    strMeasure4: string;
    strMeasure5: string;
    strMeasure6: string;
    strMeasure7: string;
    strMeasure8: string;
    strMeasure9: string;
    strMeasure10: string;
    strMeasure11: string;
    strMeasure12: string;
    strMeasure13: string;
    strMeasure14: string;
    strMeasure15: string;
    dateModified: string;
    [key: string]: string;
}


export interface RecipeState {
    recipe?: Recipe;
    isLoading: false;
    hasErrors: false;
}

export interface ActionRecipeLoading {
    type: typeof RECIPE_LOADING;
}

export interface ActionRecipeLoadError {
    type: typeof RECIPE_LOAD_ERROR;
}

export interface ActionRecipeLoaded {
    type: typeof RECIPE_LOAD_COMPLETE;
    recipe: Recipe;
}

export type ActionRecipe = ActionRecipeLoading | ActionRecipeLoadError | ActionRecipeLoaded