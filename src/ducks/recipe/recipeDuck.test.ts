import {actionRecipeLoaded, actionRecipeLoadError, actionRecipeLoading, loadRecipe} from "./recipeDuck";
import {fetchApi} from "../../utils/api";

jest.mock('../../utils/api', () => ({
    fetchApi: jest.fn(),
    LOOKUP_PATH: 'mockLookupPath'
}));
jest.mock('../../utils/transforms/recipeTransform', () => ({
    recipeTransformIn: jest.fn((value) => (`transformed ${value}`)),
}));

let dispatch;

describe('recipeDuck tests', () => {
    describe('loadRecipe thunk', () => {
        let cocktailId: number;

        beforeEach(() => {
            dispatch = jest.fn();
            cocktailId = 123;
        });

        it('should handle load recipe successful', async (next) => {
            fetchApi.mockImplementation(() => ({json: jest.fn(() => ('mockRecipe'))}));
            await loadRecipe(cocktailId)(dispatch);
            expect(dispatch).toHaveBeenCalledTimes(2);
            expect(dispatch).toHaveBeenNthCalledWith(1, actionRecipeLoading());
            expect(dispatch).toHaveBeenNthCalledWith(2, actionRecipeLoaded('transformed mockRecipe'));
            next();
        });

        it('should handle load recipe fails', async (next) => {
            fetchApi.mockImplementation(() => {throw new Error('error loading')});
            await loadRecipe(cocktailId)(dispatch);
            expect(dispatch).toHaveBeenCalledTimes(2);
            expect(dispatch).toHaveBeenNthCalledWith(1, actionRecipeLoading());
            expect(dispatch).toHaveBeenNthCalledWith(2, actionRecipeLoadError());
            next();
        });
    });
});

