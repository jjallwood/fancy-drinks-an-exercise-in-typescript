import {ThunkAction} from 'redux-thunk';
import {Action} from "redux";
import {
    ActionRecipe,
    ActionRecipeLoaded,
    ActionRecipeLoadError,
    ActionRecipeLoading,
    Recipe,
    RecipeGetResponse,
    RecipeState,
} from "./recipeDuckTypes";
import {recipeTransformIn} from "../../utils/transforms/recipeTransform";
import {fetchApi, LOOKUP_PATH} from "../../utils/api";
export const RECIPE_LOADING = 'fancyDrinks/recipe/loading';
export const RECIPE_LOAD_COMPLETE = 'fancyDrinks/recipe/loadComplete';
export const RECIPE_LOAD_ERROR = 'fancyDrinks/recipe/loadError';
export const SEARCH_INVALID = 'fancyDrinks/search/invalid';

const initialState: RecipeState = {
    isLoading: false,
    hasErrors: false,
};

export const loadRecipe = (cocktailId: number): ThunkAction<void, RecipeState, null, Action<string>> => {
    return async (dispatch): Promise<void> => {
        try {
            const searchParams = new URLSearchParams();
            searchParams.append('i', cocktailId.toString());
            dispatch(actionRecipeLoading());
            const rawResponse = await fetchApi(LOOKUP_PATH, searchParams);
            const response: RecipeGetResponse = await rawResponse.json();
            const recipe: Recipe = recipeTransformIn(response);
            dispatch(actionRecipeLoaded(recipe));

        } catch (error) {
            dispatch(actionRecipeLoadError());
        }
    };
}

export function actionRecipeLoading(): ActionRecipeLoading {
    return {type: RECIPE_LOADING};
}

export function actionRecipeLoaded(recipe: Recipe): ActionRecipeLoaded {
    return {
        type: RECIPE_LOAD_COMPLETE,
        recipe
    };
}

export function actionRecipeLoadError(): ActionRecipeLoadError {
    return { type: RECIPE_LOAD_ERROR };
}

export function reducer(state: RecipeState = initialState, action: ActionRecipe): RecipeState {
    switch (action.type) {
        case RECIPE_LOADING:
            return Object.assign({}, state, {
                recipe: undefined,
                isLoading: true,
                hasErrors: false,
            });
        case RECIPE_LOAD_COMPLETE:
            return Object.assign({}, state, {
                recipe: action.recipe,
                isLoading: false,
                hasErrors: false,
            });
        case RECIPE_LOAD_ERROR:
            return Object.assign({}, state, {
                isLoading: false,
                hasErrors: true,
            });
        default:
            return state;
    }
}