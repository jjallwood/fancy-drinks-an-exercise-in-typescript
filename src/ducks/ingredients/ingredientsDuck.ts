import {ThunkAction} from 'redux-thunk';
import {
    ActionIngredients,
    ActionIngredientsLoaded,
    ActionIngredientsLoadError,
    ActionIngredientsLoading,
    ActionIngredientsUpdateSelection,
    IngredientsGetResponse,
    IngredientsState,
    ActionIngredientsChangeFilter,
    Ingredients
} from "./ingredientsDuckTypes";
import {Action} from "redux";
import {ingredientsTransformIn} from "../../utils/transforms/ingredientsTransform";
import {fetchApi, LIST_PATH} from "../../utils/api";
export const INGREDIENTS_LOADING = 'fancyDrinks/ingredients/loading';
export const INGREDIENTS_LOAD_COMPLETE = 'fancyDrinks/ingredients/loadComplete';
export const INGREDIENTS_LOAD_ERROR = 'fancyDrinks/ingredients/loadError';
export const INGREDIENTS_UPDATE_SELECTION = 'fancyDrinks/ingredients/updateSelection';
export const INGREDIENTS_CHANGE_FILTER = 'fancyDrinks/ingredients/changeFilter';

const initialState: IngredientsState = {
    ingredients: [],
    filter: '',
    isLoading: false,
    hasErrors: false,
};

export const loadIngredients = (): ThunkAction<void, IngredientsState, null, Action<string>> => {
    return async (dispatch): Promise<void> => {
        try {
            dispatch(actionIngredientsLoading());
            const searchParams = new URLSearchParams();
            searchParams.append('i', 'list');
            const rawResponse = await fetchApi(LIST_PATH, searchParams);
            const response: IngredientsGetResponse = await rawResponse.json();
            const ingredients = ingredientsTransformIn(response);
            dispatch(actionIngredientsLoaded(ingredients));
        } catch (error) {
            dispatch(actionIngredientsLoadError());
        }
    };
}

export function actionIngredientsLoading(): ActionIngredientsLoading {
    return {type: INGREDIENTS_LOADING};
}

export function actionIngredientsLoaded(ingredients: Ingredients): ActionIngredientsLoaded {
    return {
        type: INGREDIENTS_LOAD_COMPLETE,
        ingredients
    };
}

export function actionIngredientsLoadError(): ActionIngredientsLoadError {
    return { type: INGREDIENTS_LOAD_ERROR };
}

export function actionIngredientsUpdateSelection(ingredientName: string, isSelected: boolean): ActionIngredientsUpdateSelection {
    return {
        type: INGREDIENTS_UPDATE_SELECTION,
        ingredientName: ingredientName,
        selected: isSelected,
    };
}

export function actionIngredientsChangeFilter(filter: string): ActionIngredientsChangeFilter {
    return {
        type: INGREDIENTS_CHANGE_FILTER,
        filter,
    };
}

export function reducer(state: IngredientsState = initialState, action: ActionIngredients): IngredientsState {
    switch (action.type) {
        case INGREDIENTS_LOADING:
            return Object.assign({}, state, {
                ingredients: [],
                isLoading: true,
                hasErrors: false,
            });
        case INGREDIENTS_LOAD_COMPLETE:
            return Object.assign({}, state, {
                ingredients: action.ingredients,
                isLoading: false,
                hasErrors: false,
            });
        case INGREDIENTS_LOAD_ERROR:
            return Object.assign({}, state, {
                isLoading: false,
                hasErrors: true,
            });
        case INGREDIENTS_UPDATE_SELECTION:
            const ingredients = [...state.ingredients];
            const { ingredientName } = action as ActionIngredientsUpdateSelection;
            ingredients.map(ingredient => {
                if (ingredient.name === ingredientName) {
                    ingredient.selected = !ingredient.selected;
                }
                return ingredient;
            });
            return Object.assign({}, state, {
                ingredients
            });
        case INGREDIENTS_CHANGE_FILTER:
            const { filter } = action as ActionIngredientsChangeFilter;
            return Object.assign({}, state, { filter } );
        default:
            return state;
    }
}