import {
    INGREDIENTS_CHANGE_FILTER,
    INGREDIENTS_LOAD_COMPLETE,
    INGREDIENTS_LOAD_ERROR,
    INGREDIENTS_LOADING,
    INGREDIENTS_UPDATE_SELECTION
} from "./ingredientsDuck";

export interface IngredientsGetResponse {
    drinks: Array<{
        strIngredient1: string;
    }>;
}

export type Ingredients = Array<Ingredient>;

export interface Ingredient {
    name: string;
    selected: boolean;
}

export interface IngredientsState {
    ingredients: Ingredients;
    isLoading: false;
    hasErrors: false;
    filter: string;
}

export interface ActionIngredientsLoading {
    type: typeof INGREDIENTS_LOADING;
}

export interface ActionIngredientsLoadError {
    type: typeof INGREDIENTS_LOAD_ERROR;
    ingredients?: Ingredients;
}

export interface ActionIngredientsLoaded {
    type: typeof INGREDIENTS_LOAD_COMPLETE;
    ingredients?: Ingredients;
}

export interface ActionIngredientsUpdateSelection {
    type: typeof INGREDIENTS_UPDATE_SELECTION;
    ingredientName: string;
    selected: boolean;
}

export interface ActionIngredientsChangeFilter {
    type: typeof INGREDIENTS_CHANGE_FILTER;
    filter: string;
}

export type ActionIngredients = ActionIngredientsChangeFilter | ActionIngredientsUpdateSelection | ActionIngredientsLoading | ActionIngredientsLoadError | ActionIngredientsLoaded