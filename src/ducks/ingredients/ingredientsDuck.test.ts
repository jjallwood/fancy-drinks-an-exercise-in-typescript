import {fetchApi} from "../../utils/api";
import {
    actionIngredientsLoaded,
    actionIngredientsLoadError,
    actionIngredientsLoading,
    loadIngredients
} from "./ingredientsDuck";

jest.mock('../../utils/api', () => ({
    fetchApi: jest.fn(),
    LOOKUP_PATH: 'mockLookupPath'
}));
jest.mock('../../utils/transforms/ingredientsTransform', () => ({
    ingredientsTransformIn: jest.fn((value) => (`transformed ${value}`)),
}));

let dispatch;

describe('ingredientsDuck tests', () => {
    describe('loadIngredients thunk', () => {

        beforeEach(() => {
            dispatch = jest.fn();
        });

        it('should handle loading ingredients successfully', async (next) => {
            fetchApi.mockImplementation(() => ({json: jest.fn(() => ('mockIngredients'))}));
            await loadIngredients()(dispatch);
            expect(fetchApi).toHaveBeenCalledTimes(1);
            expect(dispatch).toHaveBeenCalledTimes(2);
            expect(dispatch).toHaveBeenNthCalledWith(1, actionIngredientsLoading());
            expect(dispatch).toHaveBeenNthCalledWith(2, actionIngredientsLoaded('transformed mockIngredients'));
            next();
        });

        it('should handle when loading ingredients fails', async (next) => {
            fetchApi.mockImplementation(() => {throw new Error('error loading')});
            await loadIngredients()(dispatch);
            expect(dispatch).toHaveBeenCalledTimes(2);
            expect(dispatch).toHaveBeenNthCalledWith(1, actionIngredientsLoading());
            expect(dispatch).toHaveBeenNthCalledWith(2, actionIngredientsLoadError());
            next();
        });
    });
});

