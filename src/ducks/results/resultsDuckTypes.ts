import {SEARCH_LOAD_COMPLETE, SEARCH_LOAD_ERROR, SEARCH_LOADING} from "./resultsDuck";

export type SearchList = Array<string>

export interface CocktailGetResponse {
    drinks: Array<RawCocktail> | string;
}

export interface RawCocktail {
    strDrink: string;
    strDrinkThumb: string;
    idDrink: number;
}

export type Cocktails = Array<Cocktail>;

export interface Cocktail {
    name: string;
    img: string;
    id: number;
}

export interface ResultsState {
    cocktails: Cocktails;
    isLoading: false;
    hasErrors: false;
}

export interface ActionSearchLoading {
    type: typeof SEARCH_LOADING;
}

export interface ActionSearchLoadError {
    type: typeof SEARCH_LOAD_ERROR;
}

export interface ActionSearchLoaded {
    type: typeof SEARCH_LOAD_COMPLETE;
    cocktails: Cocktails;
}

export type ActionResults = ActionSearchLoading | ActionSearchLoadError | ActionSearchLoaded