import {fetchApi} from "../../utils/api";
import {actionSearchLoaded, actionSearchLoadError, actionSearchLoading, loadSearchResults} from "./resultsDuck";

jest.mock('../../utils/api', () => ({
    fetchApi: jest.fn(),
    LOOKUP_PATH: 'mockLookupPath'
}));
jest.mock('../../utils/transforms/resultsTransform', () => ({
    resultsTransformIn: jest.fn((value) => (`transformed ${value}`)),
}));

let dispatch;

describe('resultsDuck tests', () => {
    describe('loadSearchResults thunk', () => {
        let searchList;

        beforeEach(() => {
            dispatch = jest.fn();
            searchList = ['mockSearch1', 'mockSearch2']
        });

        it('should handle load search results successful', async (next) => {
            fetchApi.mockImplementation(() => ({json: jest.fn(() => ('mockSearchResults'))}));
            await loadSearchResults(searchList)(dispatch);
            expect(dispatch).toHaveBeenCalledTimes(2);
            expect(dispatch).toHaveBeenNthCalledWith(1, actionSearchLoading());
            expect(dispatch).toHaveBeenNthCalledWith(2, actionSearchLoaded('transformed mockSearchResults'));
            next();
        });

        it('should handle load recipe fails', async (next) => {
            fetchApi.mockImplementation(() => {throw new Error('error loading')});
            await loadSearchResults(searchList)(dispatch);
            expect(dispatch).toHaveBeenCalledTimes(2);
            expect(dispatch).toHaveBeenNthCalledWith(1, actionSearchLoading());
            expect(dispatch).toHaveBeenNthCalledWith(2, actionSearchLoadError());
            next();
        });
    });
});

