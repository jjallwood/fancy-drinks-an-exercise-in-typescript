import {ThunkAction} from 'redux-thunk';
import {Action} from "redux";
import {
    CocktailGetResponse,
    ResultsState,
    ActionResults,
    ActionSearchLoaded,
    ActionSearchLoadError, ActionSearchLoading, Cocktails
} from "./resultsDuckTypes";
import {resultsTransformIn} from "../../utils/transforms/resultsTransform";
import {fetchApi, FILTER_PATH} from "../../utils/api";
export const SEARCH_LOADING = 'fancyDrinks/search/loading';
export const SEARCH_LOAD_COMPLETE = 'fancyDrinks/search/loadComplete';
export const SEARCH_LOAD_ERROR = 'fancyDrinks/search/loadError';

const initialState: ResultsState = {
    cocktails: [],
    isLoading: false,
    hasErrors: false,
};

export const loadSearchResults = (searchList: Array<string>): ThunkAction<void, ResultsState, null, Action<string>> => {
    return async (dispatch): Promise<void> => {
        try {
            const searchParams = new URLSearchParams();
            searchParams.append('i', searchList.join(','));
            dispatch(actionSearchLoading());
            const rawResponse = await fetchApi(FILTER_PATH, searchParams);
            const response: CocktailGetResponse = await rawResponse.json();
            const results = resultsTransformIn(response);
            dispatch(actionSearchLoaded(results));
        } catch (error) {
            dispatch(actionSearchLoadError());
        }
    };
}

export function actionSearchLoading(): ActionSearchLoading {
    return {type: SEARCH_LOADING};
}

export function actionSearchLoaded(cocktails: Cocktails): ActionSearchLoaded {
    return {
        type: SEARCH_LOAD_COMPLETE,
        cocktails
    };
}

export function actionSearchLoadError(): ActionSearchLoadError {
    return { type: SEARCH_LOAD_ERROR };
}

export function reducer(state: ResultsState = initialState, action: ActionResults): ResultsState {
    switch (action.type) {
        case SEARCH_LOADING:
            return Object.assign({}, state, {
                cocktails: [],
                isLoading: true,
                hasErrors: false,
            });
        case SEARCH_LOAD_COMPLETE:
            return Object.assign({}, state, {
                cocktails: action.cocktails,
                isLoading: false,
                hasErrors: false,
            });
        case SEARCH_LOAD_ERROR:
            return Object.assign({}, state, {
                isLoading: false,
                hasErrors: true,
            });
        default:
            return state;
    }
}