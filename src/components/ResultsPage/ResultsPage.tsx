import React, {Dispatch, ReactElement} from 'react';
import {Link, withRouter} from "react-router-dom";
import CocktailCard from "../CocktailCard/CocktailCard";
import {connect} from "react-redux";
import {ResultsPageProps, ResultsPageState} from "./ResultsPageTypes";
import {CombinedReducersState} from "../../store/storeTypes";
import {loadSearchResults} from "../../ducks/results/resultsDuck";
import {Cocktail} from "../../ducks/results/resultsDuckTypes";
import Loader from "../Loader/Loader";
import ErrorMessage from "../ErrorMessage/ErrorMessage";

class ResultsPage extends React.Component<ResultsPageProps, ResultsPageState> {

    constructor(props: ResultsPageProps) {
        super(props);
        const {location: {search}} = props;
        const searchQueryParams = new URLSearchParams(search);
        const ingredientsParams = searchQueryParams.get('ingredients');
        let ingredientsList: Array<string> = [];
        if (ingredientsParams) {
            ingredientsList = ingredientsParams.split('.');
        }
        this.state = {
            ingredientsList,
        }
    }

    stringifySearchParams = (): string => {
        const {ingredientsList} = this.state;
        if (ingredientsList.length === 0) {
            return '';
        }
        if (ingredientsList.length === 1) {
            return ingredientsList[0].toLowerCase();
        }
        const length = ingredientsList.length;
        const finalIngredient = ingredientsList[length - 1].toLowerCase();
        const concatenatedIngredients = ingredientsList.slice(0, length - 1).join(', ').toLowerCase();
        return `${concatenatedIngredients} and ${finalIngredient}.`;
    };

    componentDidMount(): void {
        const {ingredientsList} = this.state;
        if (ingredientsList.length > 0) {
            this.props.loadSearchResults(ingredientsList)
        } else {
            this.props.history.push(`/`);
        }
    }

    generateCocktailClickEvent = (id: number) => (): void => {
        const searchQueryParams = new URLSearchParams();
        searchQueryParams.append('id', id.toString());
        this.props.history.push(`/recipe?${searchQueryParams.toString()}`);
    };

    render(): ReactElement {
        const {isLoading, hasErrors, cocktails = []} = this.props;
        if (isLoading){
            return <Loader />
        }
        if (hasErrors){
            return <ErrorMessage />
        }
        return (
            <div className="App">
                <h1 className="title">
                    <span className="fancy-drinks-font">{cocktails.length}</span> drinks
                    containing {this.stringifySearchParams()}
                </h1>
                {
                    cocktails.length > 0 ?
                        <div className="columns is-multiline">
                            {
                                cocktails.map(({name, img, id}: Cocktail) => (
                                    <CocktailCard
                                        key={name}
                                        name={name}
                                        img={img}
                                        onClick={this.generateCocktailClickEvent(id)}
                                    />
                                ))
                            }
                        </div>
                        :
                        <div>
                            {"Fancy that, we couldn't find anything. Why not try a different "}
                            <Link to="/">search.</Link>
                        </div>
                }
            </div>
        );
    };
};

const mapStateToProps = ({results: {cocktails, isLoading, hasErrors}}: CombinedReducersState) => ({
    cocktails,
    isLoading,
    hasErrors
});

const mapDispatchToProps = (dispatch: Dispatch<any>) => ({
    loadSearchResults: (searchList: Array<string>) => dispatch(loadSearchResults(searchList)),
});

export { ResultsPage as TestableResultsPage }

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ResultsPage));
