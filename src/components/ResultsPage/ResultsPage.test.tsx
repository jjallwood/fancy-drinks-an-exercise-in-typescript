import React from 'react';
import { shallow } from 'enzyme';
import {TestableResultsPage} from './ResultsPage';
import {ResultsPageProps} from "./ResultsPageTypes";

let props: ResultsPageProps;
let mockIngredientList: Array<string>;
let mockParamsId;

describe('ResultsPage component test', () => {
  beforeEach(() => {
    mockIngredientList = ['mockParam1', 'mockParam2', 'mockParam3'];
    mockParamsId= mockIngredientList.join('.');
    props = {
      cocktails: [
        {
          name: 'mockCocktail1',
          img: 'mockImage1',
          id: 123,
        }
      ],
      isLoading: false,
      hasErrors: false,
      loadSearchResults: jest.fn(),
      history: {push: jest.fn()} as any,
      location: {search: `ingredients=${mockParamsId}`} as any,
      match: jest.fn() as any,
    }
  });

  it('renders with data', () => {
    const wrapper = shallow(<TestableResultsPage {...props} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('loads cocktails when it has ingredients in the url params', () => {
    props.location.search = '';
    shallow(<TestableResultsPage {...props} />);
    expect(props.loadSearchResults).not.toHaveBeenCalledWith(mockIngredientList);
  });
  //
  // describe('generateCocktailClickEvent', () => {
  //   it('correctly generates event with push', () => {
  //     const wrapper = shallow(<TestableResultsPage {...props} />);
  //     const instance = wrapper.instance() as TestableResultsPage;
  //     expect(props.history.push).not.toHaveBeenCalled();
  //     instance.generateCocktailClickEvent(123);
  //     expect(props.history.push).toHaveBeenCalledWith('josh');
  //   });
  // });
});

