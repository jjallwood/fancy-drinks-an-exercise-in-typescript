import { RouteComponentProps } from "react-router-dom";
import {Cocktails} from "../../ducks/results/resultsDuckTypes";

export interface ResultsPageProps extends RouteComponentProps<any> {
    cocktails: Cocktails;
    isLoading: boolean;
    hasErrors: boolean;
    loadSearchResults: Function;
}

export interface ResultsPageState {
    ingredientsList: Array<string>;
}