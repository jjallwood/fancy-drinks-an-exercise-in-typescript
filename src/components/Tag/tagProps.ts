import {RefObject, SyntheticEvent} from "react";

export type TagProps = {
    isSelected?: boolean;
    label: string;
    onClick?: (event: SyntheticEvent ) => (void);
    ref?: RefObject<HTMLDivElement>;
}