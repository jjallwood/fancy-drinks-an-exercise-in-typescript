import posed from "react-pose";
import Tag from "./Tag";

export const PosedTag = posed(Tag)({
    enter: {y: 0, opacity: 1},
    exit: {y: 50, opacity: 0}
});