import React, {ReactElement, RefObject} from "react";
import {TagProps} from "./tagProps";
import classNames from "classnames";

export const Tag = React.forwardRef(({isSelected = false, label, onClick}: TagProps, ref): ReactElement => {
    const tagClasses = classNames('tag is-medium', {'is-dark': isSelected});
    return (
        <div ref={ref as RefObject<HTMLDivElement>} className="control" key={label}>
            <a className="tags has-addons" onClick={onClick}>
                <span className={tagClasses}>{label}</span>
                {isSelected && <div className="tag is-delete is-medium"></div>}
            </a>
        </div>
    );
});

export default Tag;