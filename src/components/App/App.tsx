import React, {ReactElement} from 'react';
import {Route, Switch} from 'react-router-dom';
import IngredientsPage from "../IngredientsPage/IngredientsPage";
import ResultsPage from "../ResultsPage/ResultsPage";
import RecipePage from "../RecipePage/RecipePage";
import NavBar from "../NavBar/NavBar";
import {PoseGroup} from 'react-pose';
import PosedRoutesContainer from "./PosedRoutesContainer";

const App: React.FC = () => {
    return (
        <Route
            render={({location}): ReactElement  => (
                <React.Fragment>
                    <NavBar/>
                    <div className="hero is-medium is-light is-bold is-fullheight-with-navbar">
                        <section className="section">
                            <div className="container">
                                <PoseGroup>
                                    <PosedRoutesContainer key={location.pathname}>
                                        <Switch location={location}>
                                            <Route exact path="/" component={IngredientsPage} key="ingredients"/>
                                            <Route exact path="/recipe" component={RecipePage} key="recipe"/>
                                            <Route exact path="/results" component={ResultsPage} key="results"/>
                                        </Switch>
                                    </PosedRoutesContainer>
                                </PoseGroup>
                            </div>
                        </section>
                    </div>
                </React.Fragment>
            )}
        />
    );
};

export default App;
