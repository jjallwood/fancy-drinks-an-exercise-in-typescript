import posed from "react-pose";

const PosedRoutesContainer = posed.div({
    enter: {opacity: 1, delay: 500, beforeChildren: true},
    exit: {opacity: 0}
});

export default PosedRoutesContainer;