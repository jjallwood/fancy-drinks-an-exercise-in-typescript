import React from 'react';

const Loader: React.FC = () => {
    return (
        <progress className="progress is-large is-primary" max="100"/>
    );
};

export default Loader;
