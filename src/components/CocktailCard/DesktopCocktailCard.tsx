import React from "react";
import {CocktailCardProps} from "./cocktailCardTypes";

const DesktopCocktailCard: React.FC<CocktailCardProps> = ({ name, img, onClick }: CocktailCardProps) => {
    return (
        <div className="card is-hidden-mobile">
            <div className="card-image">
                <figure className="image is-4by3">
                    <img src={img}
                         alt={name}/>
                </figure>
            </div>
            <div className="card-content">
                <div className="content">
                    <p className="title is-6 has-text-dark">{name}</p>
                    <a className="subtitle is-6 has-text-link" onClick={onClick}>
                        View Recipe
                    </a>
                </div>
            </div>
        </div>
    );
};

export default DesktopCocktailCard;