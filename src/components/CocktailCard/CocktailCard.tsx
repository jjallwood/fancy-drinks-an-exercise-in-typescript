import React from "react";
import {CocktailCardProps} from "./cocktailCardTypes";
import DesktopCocktailCard from "./DesktopCocktailCard";
import MobileCocktailCard from "./MobileCocktailCard";

const CocktailCard: React.FC<CocktailCardProps> = ({ name, img, onClick }: CocktailCardProps) => {
    return (
        <div className="column is-one-fifth-fullhd is-one-quarter-desktop is-one-third-tablet">
            <DesktopCocktailCard name={name} img={img} onClick={onClick} />
            <MobileCocktailCard name={name} img={img} onClick={onClick} />
        </div>
    );
};

export default CocktailCard;