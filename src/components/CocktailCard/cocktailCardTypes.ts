import {SyntheticEvent} from "react";

export interface CocktailCardProps {
    name: string;
    img: string;
    onClick: (event: SyntheticEvent ) => (void);
}