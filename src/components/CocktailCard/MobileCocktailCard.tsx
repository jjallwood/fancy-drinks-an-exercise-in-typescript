import React from "react";
import {CocktailCardProps} from "./cocktailCardTypes";

const MobileCocktailCard: React.FC<CocktailCardProps> = ({name, img, onClick}: CocktailCardProps) => {
    return (
        <div className="card is-hidden-tablet">
            <div className="card-content ">
                <a className="media" onClick={onClick}>
                    <div className="media-left">
                        <figure className="image is-48x48">
                            <img src={img}
                                 alt={name}/>
                        </figure>
                    </div>
                    <div className="media-content">
                        <span className="title is-6 has-text-dark">{name}</span>
                    </div>
                </a>
            </div>
        </div>
    );
};

export default MobileCocktailCard;