import React, {Dispatch, ReactElement} from 'react';
import {withRouter} from "react-router-dom";
import {CombinedReducersState} from "../../store/storeTypes";
import {connect} from "react-redux";
import {RecipePageProps} from "./RecipePageTypes";
import {loadRecipe} from "../../ducks/recipe/recipeDuck";
import {RecipeIngredient} from "../../ducks/recipe/recipeDuckTypes";
import Loader from "../Loader/Loader";
import ErrorMessage from "../ErrorMessage/ErrorMessage";

class RecipePage extends React.Component<RecipePageProps> {

    componentDidMount(): void {
        const searchQueryParams = new URLSearchParams(this.props.location.search);
        const recipeId = searchQueryParams.get('id');
        if (recipeId) {
            this.props.loadRecipe(recipeId)
        } else {
            this.props.history.goBack();
        }
    }

    render(): ReactElement {
        const {isLoading, hasErrors} = this.props;
        if (isLoading){
            return <Loader />
        }
        if (!hasErrors && this.props.recipe) {
            const {
                name,
                category,
                alcoholic,
                glass,
                instructions,
                img,
                ingredients,
            } = this.props.recipe;
            return (
                <React.Fragment>
                    <h1 className="title">
                        How to make a {name}
                    </h1>
                    <div className="tile is-ancestor ">
                        <div className="tile is-4 is-vertical is-parent">
                            <div className="tile is-child">
                                <article className="tile is-child">
                                    <figure className="image is-4by3">
                                        <img alt={name} src={img}/>
                                    </figure>
                                </article>
                            </div>
                            <div className="tile is-child box">
                                <p><span>Category:</span> {category}</p>
                                <p><span>Alcoholic:</span> {alcoholic}</p>
                                <p><span>Suggested Glass:</span> {glass}</p>
                            </div>
                        </div>
                        <div className="tile is-parent is-vertical">
                            <div className="tile is-child box">
                                <p className="subtitle has-text-black">Ingredients</p>
                                {
                                    ingredients.map(({name, quantity}: RecipeIngredient) => (
                                        <div key={name}>
                                            <span>{name}</span>
                                            {
                                                quantity && !!quantity.trim() &&
                                                <span className="is-italic">{` (${quantity.trim()})`}</span>
                                            }
                                        </div>
                                    ))
                                }
                            </div>
                            <div className="tile is-child box">
                                <p className="subtitle has-text-black">Instructions</p>
                                <div>
                                    {instructions}
                                </div>
                            </div>
                        </div>

                    </div>
                </React.Fragment>
            );
        }
        return <ErrorMessage />
    };
};


const mapStateToProps = ({recipe: {recipe, isLoading, hasErrors}}: CombinedReducersState
) => ({
    recipe,
    isLoading,
    hasErrors
});

const mapDispatchToProps = (dispatch: Dispatch<any>) => ({
    loadRecipe: (recipeId: number) => dispatch(loadRecipe(recipeId)),
});

export { RecipePage as TestableRecipePage };

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(RecipePage));
