import React from 'react';
import {shallow} from 'enzyme';
import {TestableRecipePage} from './RecipePage';
import {RecipePageProps} from "./RecipePageTypes";

let props: RecipePageProps;
let mockParamsId: string;

describe('RecipePage component test', () => {
    beforeEach(() => {
        mockParamsId = 'mockParamsId';
        props = {
            recipe: {
                id: 'mockId',
                dateModified: 'MockDateModified',
                name: 'MockName',
                category: 'MockCategory',
                alcoholic: 'MockAlcoholic',
                glass: 'MockGlass',
                instructions: 'MockInstructions',
                img: 'MockIng',
                ingredients: [{name: 'MocksIngredients', quantity: '4lbs'}],
            },
            isLoading: false,
            hasErrors: false,
            loadRecipe: jest.fn(),
            history: {goBack: jest.fn()} as any,
            location: {search: `id=${mockParamsId}`} as any,
            match: jest.fn() as any,
        }
    });

    it('renders with data', () => {
        const wrapper = shallow(<TestableRecipePage {...props} />);
        expect(wrapper).toMatchSnapshot();
    });

    it('loads recipes when an ID is in the url params', () => {
        shallow(<TestableRecipePage {...props} />);
        expect(props.loadRecipe).toHaveBeenCalledWith(mockParamsId);
        expect(props.history.goBack).not.toHaveBeenCalled();
    });

    it('navigated back when an ID is missing from the url params', () => {
        props.location.search = '';
        shallow(<TestableRecipePage {...props} />);
        expect(props.loadRecipe).not.toHaveBeenCalled();
        expect(props.history.goBack).toHaveBeenCalled();
    });
});

