import { RouteComponentProps } from "react-router-dom";
import {Recipe} from "../../ducks/recipe/recipeDuckTypes";

export interface RecipePageProps extends RouteComponentProps<any> {
    recipe?: Recipe;
    isLoading: boolean;
    hasErrors: boolean;
    loadRecipe: Function;
}
