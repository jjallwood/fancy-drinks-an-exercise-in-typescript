import React from 'react';
import './NavBar.scss';

const NavBar: React.FC = () => {
    return (
        <nav className="navbar" role="navigation" aria-label="main navigation">
            <div className="container">
                <div className="navbar-brand ">
                    <a className="navbar-item" href="/">
                        <h1 className={`is-8 title has-text-link fancy-drinks-font`}>
                            Fancy Drinks
                        </h1>
                    </a>
                </div>
            </div>
        </nav>
    );
};

export default NavBar;
