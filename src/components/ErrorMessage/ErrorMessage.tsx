import React from 'react';

const ErrorMessage: React.FC = () => {
    return (
        <article className="message is-danger">
            <div className="message-body">
                We are having issues connecting to the cocktails service. This is likely because the API is being blocked by CORS. Open the readme to find out more.
            </div>
        </article>
    );
};

export default ErrorMessage;
