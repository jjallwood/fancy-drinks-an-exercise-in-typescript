import {Ingredients} from "../../ducks/ingredients/ingredientsDuckTypes";
import {SyntheticEvent} from "react";

export interface SelectedIngredientsProps {
    ingredients: Ingredients;
    toggleIngredient: Function;
    onSearch: (event: SyntheticEvent) => (void);
}