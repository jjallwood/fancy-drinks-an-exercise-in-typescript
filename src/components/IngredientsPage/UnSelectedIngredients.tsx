import React, {Dispatch, ReactElement, SyntheticEvent} from 'react';
import {Ingredient} from "../../ducks/ingredients/ingredientsDuckTypes";
import {CombinedReducersState} from "../../store/storeTypes";
import {
    actionIngredientsChangeFilter,
    actionIngredientsUpdateSelection,
} from "../../ducks/ingredients/ingredientsDuck";
import {connect} from "react-redux";
import {UnSelectedIngredientsProps} from "./unSelectedIngredientsTypes";
import SearchBox from "../SearchBox/SearchBox";
import {PoseGroup} from "react-pose";
import { PosedTag } from '../Tag/PosedTag';

class UnSelectedIngredients extends React.Component<UnSelectedIngredientsProps> {
    generateTagClickEvent = (name: string, selected: boolean) => (): void => {
        const toggleSelected = !selected;
        this.props.toggleIngredient(name, toggleSelected)
    };

    render(): ReactElement {
        const {ingredients, filter, changeFilter} = this.props;
        const unselectedIngredients = ingredients.filter(ingredient => (!ingredient.selected));
        return (
            <React.Fragment>
                <SearchBox placeholder="Search ingredients" onChange={changeFilter} value={filter}/>
                {
                    filter &&
                        <div className="field is-grouped is-grouped-multiline">
                            <PoseGroup>
                            {
                                unselectedIngredients
                                    .filter(({name}: Ingredient) => {
                                        const value = name.toLowerCase().indexOf(filter.toLowerCase()) !== -1;
                                        return value;
                                    })
                                    .map(({selected, name}: Ingredient) => (
                                        <PosedTag key={name} isSelected={selected} label={name}
                                             onClick={this.generateTagClickEvent(name, selected)}/>
                                    ))
                            }
                            </PoseGroup>
                        </div>
                }

            </React.Fragment>
        );
    };
};

const mapStateToProps = ({ingredients: {ingredients, filter}}: CombinedReducersState) => ({
    ingredients,
    filter,
});

const mapDispatchToProps = (dispatch: Dispatch<any>) => ({
    toggleIngredient: (name: string, selected: boolean) => dispatch(actionIngredientsUpdateSelection(name, selected)),
    changeFilter: (syntheticEvent: SyntheticEvent) => {
        const event = syntheticEvent as React.FormEvent<HTMLInputElement>;
        const newFilter = event.currentTarget.value;
        dispatch(actionIngredientsChangeFilter(newFilter))
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(UnSelectedIngredients);
