import {Ingredients} from "../../ducks/ingredients/ingredientsDuckTypes";
import { RouteComponentProps } from "react-router-dom";

export interface IngredientsPageProps extends RouteComponentProps<void> {
    ingredients: Ingredients;
    isLoading: boolean;
    hasErrors: boolean;
    loadIngredients: Function;
    toggleIngredient: Function;
    filter: string;
}