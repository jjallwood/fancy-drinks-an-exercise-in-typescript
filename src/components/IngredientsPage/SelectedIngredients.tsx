import React, {Dispatch, ReactElement} from 'react';
import {Ingredient} from "../../ducks/ingredients/ingredientsDuckTypes";
import {CombinedReducersState} from "../../store/storeTypes";
import {
    actionIngredientsUpdateSelection,
} from "../../ducks/ingredients/ingredientsDuck";
import {connect} from "react-redux";
import {SelectedIngredientsProps} from "./selectedIngredientsTypes";
import {PoseGroup} from "react-pose";
import {PosedTag} from "../Tag/PosedTag";

class SelectedIngredient extends React.Component<SelectedIngredientsProps> {

    generateTagClickEvent = (name: string, selected: boolean) => (): void => {
        const toggleSelected = !selected;
        this.props.toggleIngredient(name, toggleSelected)
    };

    render(): ReactElement {
        const {ingredients, onSearch} = this.props;
        const selectedIngredients = ingredients.filter(ingredient => (!!ingredient.selected));
        if (selectedIngredients.length === 0) {
            return <label className='label'>Find an ingredient to get started</label>
        }
        return (
            <React.Fragment>
                <hr className="hr"/>
                <label className="label">You have chosen</label>
                <div className="field is-grouped is-grouped-multiline">
                    <PoseGroup>
                        {
                            selectedIngredients
                                .map(({selected, name}: Ingredient) => (
                                    <PosedTag ref={React.createRef()} key={name} isSelected={selected} label={name}
                                              onClick={this.generateTagClickEvent(name, selected)}/>
                                ))
                        }
                    </PoseGroup>
                </div>
                <button className="button is-link" onClick={onSearch}>Find drinks with these ingredients</button>
            </React.Fragment>
        );
    };
};

const mapStateToProps = ({ingredients: {ingredients}}: CombinedReducersState) => ({
    ingredients,
});

const mapDispatchToProps = (dispatch: Dispatch<any>) => ({
    toggleIngredient: (name: string, selected: boolean) => dispatch(actionIngredientsUpdateSelection(name, selected)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SelectedIngredient);
