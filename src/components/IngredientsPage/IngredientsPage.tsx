import React, {Dispatch, ReactElement} from 'react';
import {withRouter} from "react-router";
import {CombinedReducersState} from "../../store/storeTypes";
import {
    loadIngredients
} from "../../ducks/ingredients/ingredientsDuck";
import {IngredientsPageProps} from "./IngredientsPageTypes";
import {connect} from "react-redux";
import SelectedIngredients from "./SelectedIngredients";
import UnSelectedIngredients from "./UnSelectedIngredients";
import Loader from "../Loader/Loader";
import ErrorMessage from "../ErrorMessage/ErrorMessage";

class IngredientsPage extends React.Component<IngredientsPageProps> {

    componentDidMount(): void {
        if (this.props.ingredients.length === 0) {
            this.props.loadIngredients()
        }
    }

    onSearch = (): void => {
        const searchQueryParams = new URLSearchParams();
        const {ingredients} = this.props;
        const ingredientList = ingredients
            .filter((({selected}) => (!!selected)))
            .map(({name}) => (name))
            .join('.');
        searchQueryParams.append('ingredients', ingredientList);
        this.props.history.push(`/results?${searchQueryParams}`);
    };

    render(): ReactElement {
        const {isLoading, hasErrors} = this.props;
        if (isLoading){
            return <Loader />
        }
        if (hasErrors){
            return <ErrorMessage />
        }
        return (
            <div className="App">
                <h1 className="title">
                    What do you <span className="fancy-drinks-font">Fancy</span> drinking?
                </h1>
                <UnSelectedIngredients />
                <SelectedIngredients onSearch={this.onSearch} />
            </div>
        );
    };
};

const mapStateToProps = ({ingredients: {ingredients, isLoading, hasErrors}}: CombinedReducersState) => ({
    isLoading,
    hasErrors,
    ingredients,
});

const mapDispatchToProps = (dispatch: Dispatch<any>) => ({
    loadIngredients: () => dispatch(loadIngredients()),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(IngredientsPage));
