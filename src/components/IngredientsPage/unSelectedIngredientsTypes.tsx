import {Ingredients} from "../../ducks/ingredients/ingredientsDuckTypes";
import {SyntheticEvent} from "react";

export interface UnSelectedIngredientsProps {
    ingredients: Ingredients;
    filter: string;
    toggleIngredient: Function;
    changeFilter: (event: SyntheticEvent) => (void);
}