import {SyntheticEvent} from "react";

export interface SearchBoxProps {
    placeholder: string;
    value: string;
    onChange: (event: SyntheticEvent ) => (void);
}