import React from "react";
import {SearchBoxProps} from "./searchBoxTypes";

const SearchBox: React.FC<SearchBoxProps> = ({ placeholder, value, onChange }: SearchBoxProps) => {
    return (
        <div className="field">
            <input
                className="input is-rounded"
                type="text"
                placeholder={placeholder}
                value={value}
                onChange={onChange}
            />
        </div>
    );
};

export default SearchBox;