import {Ingredients, IngredientsGetResponse} from "../../ducks/ingredients/ingredientsDuckTypes";

export const ingredientsTransformIn = (response: IngredientsGetResponse): Ingredients => {
    const drinks = response.drinks;
    if (Array.isArray(drinks)) {
        return drinks.map(
            ({strIngredient1}) => ({name: strIngredient1, selected: false})
        );
    }
    throw new Error('Failed to load recipe');
}