import {CocktailGetResponse, Cocktails, RawCocktail} from "../../ducks/results/resultsDuckTypes";

export const resultsTransformIn = (response: CocktailGetResponse): Cocktails => {
    const drinks = response.drinks;
    if (Array.isArray(drinks)) {
        return drinks.map(
            ({strDrink, strDrinkThumb, idDrink}: RawCocktail) => (
                {id: idDrink, name: strDrink, img: strDrinkThumb}
            )
        );
    };
    throw new Error('Failed to load recipe');
}