import {RawRecipe, Recipe, RecipeGetResponse, RecipeIngredient} from "../../ducks/recipe/recipeDuckTypes";

export const recipeTransformIn = (response: RecipeGetResponse): Recipe => {
    if (Array.isArray(response.drinks) && response.drinks.length > 0) {
        const rawRecipe: RawRecipe = response.drinks[0];
        const {
            idDrink: id,
            strCategory: category,
            strDrink: name,
            strAlcoholic: alcoholic,
            strGlass: glass,
            strInstructions: instructions,
            strDrinkThumb: img,
            dateModified,
        } = rawRecipe;
        const recipeIngredients: Array<RecipeIngredient> = [];
        for (let ingredientIndex = 0; ingredientIndex < 15; ingredientIndex++) {
            const ingredientAttribute = `strIngredient${ingredientIndex}`;
            const ingredientName = rawRecipe[ingredientAttribute]
            if (ingredientName) {
                const ingredientQuantity = rawRecipe[`strMeasure${ingredientIndex}`];
                const ingredient: RecipeIngredient = {
                    name: ingredientName,
                    quantity: ingredientQuantity,
                };
                recipeIngredients.push(ingredient);
            }
        }
        return {
            id, category, name, alcoholic, glass, instructions, img, dateModified,
            ingredients: recipeIngredients,
        }
    }
    throw new Error('Failed to load recipe');
}