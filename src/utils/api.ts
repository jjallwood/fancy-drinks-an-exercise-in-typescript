const BASE_API = 'https://www.thecocktaildb.com/api/json/v2/1/';
const API_SECRET = '9973533'; //this should really go anywhere else, i.e on the node process runtime

export const LIST_PATH = 'list.php';
export const FILTER_PATH = 'filter.php';
export const LOOKUP_PATH = 'lookup.php';

export const fetchApi = async (path: string, searchParams: URLSearchParams = new URLSearchParams() ) => {
    return fetch(`${BASE_API}${path}?${searchParams.toString()}`, {
        credentials: 'include',
        headers: {
            'Authorization': `Bearer ${API_SECRET}`,
        },
        method: 'get',
    });
};