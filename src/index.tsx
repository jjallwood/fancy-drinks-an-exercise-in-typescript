import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import './bulma.scss';
import App from './components/App/App';
import configureStore from "./store/store";
import {BrowserRouter} from "react-router-dom";

ReactDOM.render(
    <Provider store={configureStore()}>
        <BrowserRouter>
            <App/>
        </BrowserRouter>
    </Provider>,
    document.getElementById('root')
);

