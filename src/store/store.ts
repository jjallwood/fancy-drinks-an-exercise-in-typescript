import { createStore, applyMiddleware, combineReducers } from 'redux';
import { reducer as ingredients } from "../ducks/ingredients/ingredientsDuck";
import { reducer as results } from "../ducks/results/resultsDuck";
import { reducer as recipe } from "../ducks/recipe/recipeDuck";
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

const rootReducer = combineReducers({
    ingredients,
    results,
    recipe,
});

export default function configureStore() {
    return createStore(
        rootReducer,
        {},
        composeWithDevTools(
            applyMiddleware(thunk),
            // other store enhancers if any
        ),
    );
}