import {IngredientsState} from "../ducks/ingredients/ingredientsDuckTypes";
import {ResultsState} from "../ducks/results/resultsDuckTypes";
import {RecipeState} from "../ducks/recipe/recipeDuckTypes";

export interface CombinedReducersState {
    ingredients: IngredientsState;
    results: ResultsState;
    recipe: RecipeState;
}