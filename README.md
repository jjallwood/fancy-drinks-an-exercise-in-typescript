# Fancy Drinks - An Exercise in Typescript

## Setup
1. Clone
1. `npm install`
1. `npm start`
1. Due to my unfortunate choice of API, you will need to run your browser with CORS disabled for the fetch to work. ([Instructions for Chrome](https://alfilatov.com/posts/run-chrome-without-cors/))
1. `http:localhost:3000`

## Things that went well 
* TypesScript project
* Bulma styling
* Responsive
* Pose react animations, especially on the tags.
* No lint errors, (just a few warnings)

## Things that could have gone better
* Test coverage is in place, but just some example coverage. I should remember it can take as long to write unit tests as it can to write the components in the first place. 
* URL driven navigation (used) vs State / Store driven navigation. 
    * The current implementation keeps a clean seperation of responsibility but makes it harder to integrate data between pages.
    * By keeping some of the information in the URL, my high level components needed to read the params. Usually I would prefer to keep components stateless.
* The API doesn't have CORS configured correctly, despite being a public API.
* The API wasn't restful.
* The API had an odd way of displaying errors, I had to handle these strange responses in my Interfaces.
* By not writing seperate interfaces for `mapStateToProps` and `mapDispatchToProps` I have about 20 lint errors need to be fixed.
* Since all my reducers have similar loading and error states, it would be good to create a HOC to apply these to the pages consistently.